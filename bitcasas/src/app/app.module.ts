import { HttpClientModule  } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SellerpropComponent } from './sellerprop/sellerprop.component';
import { HeaderComponent } from './header/header.component';
import { FootersectionComponent } from './footersection/footersection.component';

@NgModule({
  declarations: [
    AppComponent,
    SellerpropComponent,
    HeaderComponent,
    FootersectionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
