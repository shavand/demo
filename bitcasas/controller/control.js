const mysql = require('mysql');

connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  port:'9090',
  password: '',
  database: 'mydb'
});



let userModel = {};

userModel.getUsers = (callback) => {
  if (connection) {
      
    connection.query("SELECT t1.*,t2.*,t3.*,t4.*,t6.*,t1.auto_id as product_id, t2.status as borrower_status,t3.status as lon_status,t4.status as payment_status,t6.status as document_status from property_info as t1 inner join borrower_info as t2 on t2.property_id=t1.auto_id inner join lon_info as t3 on t3.property_id=t1.auto_id inner join payment as t4 on t4.property_id=t1.auto_id inner join documents as t6 on t6.property_id='" + 16 + "'",
      (err, rows) => {
        if (err) {
          throw err
        }
        else {
          callback(null, rows);
        }
      }
    )
  }
  
};

module.exports = userModel;